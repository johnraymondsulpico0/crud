defmodule Crud.ArticlesFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Crud.Articles` context.
  """

  @doc """
  Generate a article.
  """
  def article_fixture(attrs \\ %{}) do
    {:ok, article} =
      attrs
      |> Enum.into(%{
        content: "some content",
        created_date: ~N[2022-07-25 07:01:00],
        title: "some title"
      })
      |> Crud.Articles.create_article()

    article
  end
end
