defmodule Crud.Repo.Migrations.CreateAticles do
  use Ecto.Migration

  def change do
    create table(:aticles) do
      add :title, :string
      add :created_date, :naive_datetime
      add :content, :text

      timestamps()
    end
  end
end
