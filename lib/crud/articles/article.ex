defmodule Crud.Articles.Article do
  use Ecto.Schema
  import Ecto.Changeset

  schema "aticles" do
    field :content, :string
    field :created_date, :naive_datetime
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(article, attrs) do
    article
    |> cast(attrs, [:title, :created_date, :content])
    |> validate_required([:title, :content])
  end
end
